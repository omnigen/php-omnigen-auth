<?php


namespace Omnigen\Auth;


use Omnigen\Auth\Exceptions\InvalidCredentialsException;
use Omnigen\Auth\Exceptions\InvalidHttpMethodException;
use Omnigen\Auth\Structs\BaseUser;


class AuthApi
{
    const HTTP_METHODS = ['GET', 'POST', 'DELETE'];
    const ENDPOINT_VALIDATE_ACCESSTOKEN = 'api/validate';
    const ENDPOINT_REVOKE_ACCESSTOKEN = 'api/revoke';
    const ENDPOINT_INVITE_USER = 'api/invite-user';
    const ENDPOINT_LIST_USERS = 'api/list-users';
    const ENDPOINT_REMOVE_USER = 'api/remove-user';
    const ENDPOINT_LIST_SIGNUPS = 'api/list-signups';
    const ENDPOINT_GET_USER = 'api/get-user';
    const ENDPOINT_REVOKE_SIGNUP = 'api/revoke-signup';
    const ENDPOINT_LOCK_USER = 'api/lock-user';
    const ENDPOINT_UNLOCK_USER = 'api/unlock-user';
    const ENDPOINT_SEARCH = 'api/search';

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string|null
     */
    private $authHeader = null;


    /**
     * AuthApi constructor.
     * @param string $baseUrl
     * @param string|null $consumerId
     * @param string $consumerSecret
     */
    public function __construct(string $baseUrl, string $consumerId = null, string $consumerSecret = null)
    {
        if (!Utils::stringEndsWith($baseUrl, '/')) {
            $baseUrl .= '/';
        }
        $this->baseUrl = $baseUrl;
        if ($consumerId === null || $consumerSecret == null) {
            throw new InvalidCredentialsException();
        } else {
            $encodedCredentials = base64_encode(
                sprintf('%s:%s', $consumerId, $consumerSecret));
            $this->authHeader = 'Basic ' . $encodedCredentials;
        }
    }

    /**
     * @param string $endpoint
     * @param string $method
     * @param array $headers
     * @param array $queryParams
     * @return array
     */
    private function makeRequest(string $endpoint, string $method = 'GET', array $headers = [], array $queryParams = []): array
    {
        if (Utils::stringStartsWith($endpoint, '/')) {
            $endpoint = substr($endpoint, 1);
        }

        if (!in_array($method, self::HTTP_METHODS)) {
            throw new InvalidHttpMethodException($method);
        }

        if ($this->authHeader !== null)
            $headers['Authorization'] = $this->authHeader;

        $url = $this->baseUrl . $endpoint;
        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams);
        }

        $curlSession = curl_init($url);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_CUSTOMREQUEST, $method);
        if (!empty($headers)) {
            $headerArray = [];
            foreach ($headers as $key => $value) {
                $headerArray[] = $key . ': ' . $value;
            }
            curl_setopt($curlSession, CURLOPT_HTTPHEADER, $headerArray);
        }

        $response = curl_exec($curlSession);
        $statusCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE);
        curl_close($curlSession);

        return [
            'response' => $response,
            'status' => $statusCode
        ];
    }

    /**
     * @param string $accessToken
     * @return array
     */
    public function validateAccessToken(string $accessToken): array
    {
        $queryParams = ['access_token' => $accessToken];
        return $this->makeRequest(self::ENDPOINT_VALIDATE_ACCESSTOKEN, 'GET', [], $queryParams);
    }

    /**
     * @param string $accessToken
     * @return array
     */
    public function revokeAccessToken(string $accessToken): array
    {
        $queryParams = ['access_token' => $accessToken];
        return $this->makeRequest(self::ENDPOINT_REVOKE_ACCESSTOKEN, 'GET', [], $queryParams);
    }

    /**
     * @param string $clientId
     * @param string $email
     * @return array
     */
    public function inviteUser(string $clientId, string $email): array
    {
        $queryParams = ['client_id' => $clientId, 'email' => $email];
        return $this->makeRequest(self::ENDPOINT_INVITE_USER, 'POST', [], $queryParams);
    }

    /**
     * @param string $clientId
     * @return array
     */
    public function listUsers(string $clientId): array
    {
        $queryParams = ['client_id' => $clientId];
        return $this->makeRequest(self::ENDPOINT_LIST_USERS, 'GET', [], $queryParams);
    }

    /**
     * @param string $uid
     * @return array
     */
    public function getUser(string $uid): array
    {
        $queryParams = ['user_uid' => $uid];
        return $this->makeRequest(self::ENDPOINT_GET_USER, 'GET', [], $queryParams);
    }

    /**
     * @param string $uid
     * @param bool $returnDummy
     * @return BaseUser
     */
    public function getUserObject(string $uid, bool $returnDummy = false): BaseUser
    {
        $response = $this->getUser($uid);
        if ($response['status'] !== 200)
            return Utils::getInvalidResponse($response['status'], $returnDummy);

        $data = json_decode($response['response'], true);
        if (!$data)
            return Utils::getInvalidResponse($response['status'], $returnDummy);
        $data['valid'] = true;
        return BaseUser::factory($data);
    }

    /**
     * @param string $uid
     * @param bool $sendGdprMail
     * @return array
     */
    public function removeUser(string $uid, bool $sendGdprMail = true): array
    {
        $queryParams = ['uid' => $uid, 'send_gdpr_mail' => $sendGdprMail];
        return $this->makeRequest(self::ENDPOINT_REMOVE_USER, 'DELETE', [], $queryParams);
    }

    /**
     * @param string $clientId
     * @return array
     */
    public function listSignups(string $clientId): array
    {
        $queryParams = ['client_id' => $clientId];
        return $this->makeRequest(self::ENDPOINT_LIST_SIGNUPS, 'GET', [], $queryParams);
    }

    /**
     * @param string $uid
     * @return array
     */
    public function lockUser(string $uid): array
    {
        $queryParams = ['uid' => $uid];
        return $this->makeRequest(self::ENDPOINT_LOCK_USER, 'POST', [], $queryParams);
    }

    /**
     * @param string $uid
     * @return array
     */
    public function unlockUser(string $uid): array
    {
        $queryParams = ['uid' => $uid];
        return $this->makeRequest(self::ENDPOINT_UNLOCK_USER, 'POST', [], $queryParams);
    }

    public function search(string $query, int $offset = -1, int $maxItems = -1, array $fields = null,
                           string $clientId = null): array
    {
        $queryParams = ['query' => $query];
        if ($offset >= 0) $queryParams['offset'] = $offset;
        if ($maxItems >= 0) $queryParams['max_items'] = $maxItems;
        if (!empty($fields)) $queryParams['fields'] = implode(',', $fields);
        if (!empty($clientId)) $queryParams['client_id'] = $clientId;

        return $this->makeRequest(self::ENDPOINT_SEARCH, 'GET', [], $queryParams);
    }
}
