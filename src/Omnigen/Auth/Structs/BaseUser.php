<?php


namespace Omnigen\Auth\Structs;


use Omnigen\Auth\Exceptions\InvalidTypeException;

abstract class BaseUser
{
    /**
     * @var string|null
     */
    private $uid = null;

    /**
     * @var string|null
     */
    private $clientId = null;

    /**
     * @var array
     */
    private $scopes = [];

    /**
     * @var bool
     */
    private $valid;

    /**
     * BaseUser constructor.
     * @param bool $valid
     * @param array $data
     */
    public function __construct(bool $valid, array $data)
    {
        $expectedType = $this->getExpectedType();
        $this->valid = $valid;
        if ($valid) {
            if ($expectedType !== $data['type'])
                throw new InvalidTypeException($expectedType, $data['type']);

            if (isset($data['uid']))
                $this->uid = $data['uid'];
            $this->clientId = $data['client_id'];
            $this->scopes = preg_split('/\s+/', $data['scopes'], -1, PREG_SPLIT_NO_EMPTY);
        }
    }

    /**
     * @return string|null
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return array
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @return bool
     */
    public function isUserLoggedIn(): bool
    {
        return $this->isValid() && $this->getUid() !== null;
    }

    /**
     * @return bool
     */
    public function isClientLoggedIn(): bool
    {
        $isClient = $this->getClientId() !== null && $this->isValid();
        return $isClient && !$this->isUserLoggedIn();
    }

    /**
     * @return string
     */
    protected abstract function getExpectedType(): string;

    /**
     * @param array $data
     * @return BaseUser
     */
    public static function factory(array $data): BaseUser
    {
        if (!$data['valid']) return new User(false, $data);

        $type = $data['data']['type'];
        if ($type === 'user') return new User(true, $data['data']);
        else if ($type === 'client') return new Client(true, $data['data']);

        throw new InvalidTypeException('user|client', $type);
    }
}
