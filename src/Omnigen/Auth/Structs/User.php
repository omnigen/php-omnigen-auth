<?php


namespace Omnigen\Auth\Structs;


class User extends BaseUser
{
    /**
     * @var string|null
     */
    private $email = null;

    /**
     * @var bool
     */
    private $locked = true;

    /**
     * @var string|null
     */
    private $firstName = null;

    /**
     * @var string|null
     */
    private $lastName = null;

    /**
     * @var string|null
     */
    private $username = null;

    /**
     * @var string|null
     */
    private $locale = null;

    public function __construct(bool $valid, array $data)
    {
        parent::__construct($valid, $data);
        if ($valid) {
            $this->email = $data['email'];
            $this->locked = $data['locked'];

            $this->firstName = self::fetchFromArray($data, 'first_name');
            $this->lastName = self::fetchFromArray($data, 'last_name');
            $this->username = self::fetchFromArray($data, 'username');
            $this->locale = self::fetchFromArray($data, 'locale');
        }
    }

    private static function fetchFromArray(array $data, string $key)
    {
        if (isset($data[$key]))
            return $data[$key];
        return null;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    protected function getExpectedType(): string
    {
        return 'user';
    }
}
