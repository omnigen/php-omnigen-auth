<?php


namespace Omnigen\Auth\Structs;


class Client extends BaseUser
{
    /**
     * @return string
     */
    protected function getExpectedType(): string
    {
        return 'client';
    }
}
