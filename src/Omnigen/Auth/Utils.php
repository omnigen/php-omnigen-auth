<?php


namespace Omnigen\Auth;


use Omnigen\Auth\Exceptions\InvalidResponseException;
use Omnigen\Auth\Structs\User;

class Utils
{
    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function stringStartsWith(string $haystack, string $needle): bool
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function stringEndsWith(string $haystack, string $needle): bool
    {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }

    public static function getInvalidResponse(int $statusCode, bool $returnDummy = true)
    {
        if ($returnDummy)
            return new User(False, []);
        throw new InvalidResponseException($statusCode);
    }

}
