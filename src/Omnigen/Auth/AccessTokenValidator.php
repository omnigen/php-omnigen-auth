<?php


namespace Omnigen\Auth;


use Omnigen\Auth\Structs\BaseUser;

class AccessTokenValidator
{
    /**
     * @var AuthApi
     */
    private $authApi;

    /**
     * @var bool
     */
    private $returnDummy;

    /**
     * AccessTokenValidator constructor.
     * @param string $baseUrl
     * @param string|null $consumerId
     * @param string|null $consumerSecret
     * @param bool $returnDummy
     */
    public function __construct(string $baseUrl, string $consumerId = null, string $consumerSecret = null,
                                bool $returnDummy = true)
    {
        $this->authApi = new AuthApi($baseUrl, $consumerId, $consumerSecret);
        $this->returnDummy = $returnDummy;
    }

    /**
     * @return AuthApi
     */
    public function getApiInterface(): AuthApi
    {
        return $this->authApi;
    }

    /**
     * @param string $accessToken
     * @return BaseUser
     */
    public function getAccessTokenData(string $accessToken): BaseUser
    {
        $response = $this->authApi->validateAccessToken($accessToken);
        if ($response['status'] !== 200)
            return Utils::getInvalidResponse($response['status'], $this->returnDummy);

        $data = json_decode($response['response'], true);
        if (!$data)
            return Utils::getInvalidResponse($response['status'], $this->returnDummy);
        return BaseUser::factory($data);
    }

    /**
     * @param string $accessToken
     * @return bool
     */
    public function isAccessTokenValid(string $accessToken): bool
    {
        return $this->getAccessTokenData($accessToken)->isValid();
    }
}
