<?php


namespace Omnigen\Auth\Exceptions;


class InvalidTypeException extends \RuntimeException
{

    public function __construct(string $expectedType, string $receivedType)
    {
        parent::__construct(
            sprintf('Expected \'%s\' type, got \'%s\'', $expectedType, $receivedType), 0, null);
    }
}
