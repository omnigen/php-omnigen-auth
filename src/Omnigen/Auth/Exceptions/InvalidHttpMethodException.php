<?php


namespace Omnigen\Auth\Exceptions;


use Omnigen\Auth\AuthApi;

class InvalidHttpMethodException extends \RuntimeException
{

    public function __construct(string $method)
    {
        parent::__construct(
            sprintf('HTTP method %s is not supported. Choose one of %s', $method, implode(', ', AuthApi::HTTP_METHODS)),
            0, null);
    }
}
