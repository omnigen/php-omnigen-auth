<?php


namespace Omnigen\Auth\Exceptions;


class InvalidCredentialsException extends \RuntimeException
{

    public function __construct()
    {
        parent::__construct('No credentials provided, not running as app engine project', 0, null);
    }
}
