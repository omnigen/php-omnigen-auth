<?php


namespace Omnigen\Auth\Exceptions;


class InvalidResponseException extends \RuntimeException
{

    public function __construct($statusCode)
    {
        parent::__construct(sprintf('Unexpected response (%d)', $statusCode), 0, null);
    }
}
